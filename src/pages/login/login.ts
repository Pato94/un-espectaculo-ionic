import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Facebook } from 'ionic-native';
import { TabsPage } from '../tabs/tabs';

@Component({
  templateUrl: 'login.html'
})
export class LoginPage {
  constructor(public navCtrl: NavController) {
      this.navCtrl = navCtrl;
  }

  loginFacebook() {
    Facebook.login(['public_profile', 'email'])
    .then(rta => {
      console.log(rta.status)
      if(rta.status == 'conneced'){
        Facebook.api('/me?fields=id,name,email,first_name,last_name,gender', 
        ['public_profile', 'email'])
        .then(rta => {
          alert(rta.id);
          //this.openApp(rta);
        })
        .catch(error => {
          alert(error.msg);
          //console.error( error );
        });
      };
    })
    .catch(error => {
      alert(error.msg);
      console.error( error );
    });
  }

  openApp(res: any) {
    this.navCtrl.push(TabsPage, { value: res });
  }
}